CREATE TABLE todo (
    id UUID NOT NULL PRIMARY KEY,
    task VARCHAR(100) NOT NULL,
    isClear BOOLEAN NOT NULL,
    createdAt VARCHAR(100) NOT NULL,
    updatedAt VARCHAR(100) NOT NULL
)