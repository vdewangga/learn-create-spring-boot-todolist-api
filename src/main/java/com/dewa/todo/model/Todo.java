package com.dewa.todo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

public class Todo {
    private final UUID id;

    @NotBlank
    private final String task;

    private final Boolean isClear;


    private final String createdAt;
    private final String updatedAt;

    public Todo(@JsonProperty("id") UUID id,
                @JsonProperty("task") @NotBlank String task,
                @JsonProperty("isClear") Boolean isClear,
                @JsonProperty("createdAt") String createdAt,
                @JsonProperty("updatedAt") String updatedAt) {
        this.id = id;
        this.task = task;
        this.isClear = isClear;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public UUID getId() {
        return id;
    }

    public String getTask() {
        return task;
    }

    public Boolean getClear() {
        return isClear;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
