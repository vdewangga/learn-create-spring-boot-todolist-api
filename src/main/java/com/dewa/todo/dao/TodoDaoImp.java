package com.dewa.todo.dao;

import com.dewa.todo.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("postgres")
public class TodoDaoImp implements TodoDao{
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TodoDaoImp(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertTodo(UUID id, Todo todo, Boolean isClear, String createdAt, String updatedAt) {
        final String sql = "INSERT INTO todo (id, task, isClear, createdAt, updatedAt) VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, id, todo.getTask(), isClear, createdAt, updatedAt);
        return 1;
    }

    @Override
    public List<Todo> showTodo() {
        final String sql = "SELECT id, task, createdAt, updatedAt, isClear FROM todo";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String task = resultSet.getString("task");
            Boolean isClear = resultSet.getBoolean("isClear");
            String createdAt = resultSet.getString("createdAt");
            String updatedAt = resultSet.getString("updatedAt");
            return new Todo(id, task, isClear, createdAt, updatedAt);
        });
    }

    @Override
    public List<Todo> selectTodo(UUID id) {
        final String sql = "SELECT id, task, createdAt, updatedAt, isClear FROM todo WHERE id = ?";
        return jdbcTemplate.query(sql, new Object[]{id}, (resultSet, i) -> {
            UUID todoId = UUID.fromString(resultSet.getString("id"));
            String task = resultSet.getString("task");
            Boolean isClear = resultSet.getBoolean("isClear");
            String createdAt = resultSet.getString("createdAt");
            String updatedAt = resultSet.getString("updatedAt");
            return new Todo(todoId, task, isClear, createdAt, updatedAt);
        });
    }

    @Override
    public int deleteTodo(UUID id) {
        final String sql = "DELETE FROM todo WHERE id = ? ";
        jdbcTemplate.update(sql, id);
        return 1;
    }

    @Override
    public int updateTodo(UUID id, Todo todo, Boolean isClear, String updatedAt) {
        final String sql = "UPDATE todo SET task = ?, isClear = ?, updatedAt = ? WHERE id = ?";
        jdbcTemplate.update(sql, todo.getTask(), isClear, updatedAt, id);
        return 1;
    }
}
