package com.dewa.todo.dao;

import com.dewa.todo.model.Todo;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface TodoDao {

    int insertTodo(UUID id, Todo todo, Boolean isClear, String createdAt, String updatedAt);

    default int insertTodo(Todo todo) {
        UUID id = UUID.randomUUID();
        String createdAt = Long.toString(Instant.now().toEpochMilli());
        String updatedAt = Long.toString(Instant.now().toEpochMilli());
        return insertTodo(id, todo, false, createdAt, updatedAt);
    }

    List<Todo> showTodo();

    List<Todo> selectTodo(UUID id);

    int deleteTodo(UUID id);

    int updateTodo(UUID id, Todo todo, Boolean isClear, String updatedAt);
    default int updateTodo(UUID id, Todo todo, Boolean isClear) {
        String updatedAt = Long.toString(Instant.now().toEpochMilli());
        return updateTodo(id, todo, isClear, updatedAt);
    }
}
