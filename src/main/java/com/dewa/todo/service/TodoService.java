package com.dewa.todo.service;

import com.dewa.todo.dao.TodoDao;
import com.dewa.todo.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class TodoService {
    private final TodoDao todoDao;

    @Autowired
    public TodoService(@Qualifier("postgres") TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    public Map<String, Object> insertTask(Todo todo) {
        Map<String, Object> replay = new HashMap<>();
        replay.put("code", 200);
        replay.put("message", "success");
        todoDao.insertTodo(todo);
        return replay;
    }

    public Map<String, Object> showTodo() {
        List<Todo> todoList = todoDao.showTodo();
        Map<String, Object> replay = new HashMap<>();
        replay.put("code", 200);
        replay.put("message", "success");
        replay.put("data", todoList);
        return replay;
    }

    public Map<String, Object> selectTodoById(UUID id) {
        List<Todo> todoList = todoDao.selectTodo(id);
        Map<String, Object> replay = new HashMap<>();
        replay.put("code", 200);
        replay.put("message", "success");
        replay.put("data", todoList);
        return replay;
    }

    public Map<String, Object> updateTodo(UUID id, Todo todo) {
        Boolean isClear = todo.getClear();
        Map<String, Object> replay = new HashMap<>();
        replay.put("code", 200);
        replay.put("message", "success");
        todoDao.updateTodo(id, todo, isClear);
        return replay;
    }

    public Map<String, Object> deleteTodo(UUID id) {
        Map<String, Object> replay = new HashMap<>();
        replay.put("code", 200);
        replay.put("message", "success");
        todoDao.deleteTodo(id);
        return replay;
    }
}
