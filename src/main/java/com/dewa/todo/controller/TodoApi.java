package com.dewa.todo.controller;

import com.dewa.todo.model.Todo;
import com.dewa.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RequestMapping("api/v1/todo")
@RestController
public class TodoApi {
    private final TodoService todoService;

    @Autowired
    public TodoApi(TodoService todoService) {
        this.todoService = todoService;
    }

    @PostMapping
    public Map<String, Object> addTask(@RequestBody Todo todo) {
        return todoService.insertTask(todo);
    }

    @GetMapping
    public Map<String, Object> showTodos() {
        return todoService.showTodo();
    }

    @GetMapping(path = "{id}")
    public Map<String, Object> selectTodoById(@PathVariable("id") UUID id) {
        return todoService.selectTodoById(id);
    }

    @PutMapping(path = "{id}")
    public Map<String, Object> updateTodo(@PathVariable("id") UUID id, @RequestBody @Valid @NotNull Todo todo) {
        return todoService.updateTodo(id, todo);
    }

    @DeleteMapping(path = "{id}")
    public Map<String, Object> deleteTodo(@PathVariable("id") UUID id) {
        return todoService.deleteTodo(id);
    }
}
